+++
title = "Shutting down services"
date = 2021-10-01

[extra]
desc = "Due to financial issues, services will start shutting down."
img = "/img/blog-default.svg"
+++

## Why?

Due to financial issues, services will start shutting down. This is not due to not having money, but having a way to use it.
It is impossible for me to access financial services like bank accounts and this forces me to use prepaid cards which is not sustainable because of
currency conversions and the minimum credit purchase on Fly.io being $25. This would make me buy a $50 CAD card to reload $25 USD on Fly which is about $30 CAD
at the time of writing this, leaving me with $20 CAD on a card that I don't have any use for.

## The plan
Tor hidden services will continue to work. Redirects will be setup to avoid breaking links until January 2021. Librarian will still
be available until more community instances are hosted, similar to the invidio.us shutdown.

| Service    | Plan                                           |
|------------|------------------------------------------------|
| Librarian  | More unreliable hosting on home server         |
| Libreddit  | Clearnet and hidden service shutdown           |
| Nitter     | Clearnet shutdown, no change to hidden service |
| Whoogle    | Complete shutdown                              |
| Bibliogram | No change                                      |
| IPFS Apps  | No change                                      |

## Alternatives
These projects have many other community hosted instances. Lists are linked below.

| Service    | List                                                                                  |
|------------|---------------------------------------------------------------------------------------|
| Librarian  | [Codeberg](https://codeberg.org/imabritishcow/librarian#instances)                    |
| Libreddit  | [GitHub](https://github.com/spikecodes/libreddit#instances)                           |
| Nitter     | [GitHub](https://github.com/zedeus/nitter/wiki/Instances)                             |
| Whoogle    | [GitHub](https://github.com/benbusby/whoogle-search#public-instances)                 |
| Bibliogram | [Sourcehut](https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md) |