+++
title = "Introducing the next bcow.xyz"
date = 2022-05-26

[extra]
desc = "Introducing new design and branding and a new, modern experience."
img = "/img/blog-default.svg"
+++

This website started as a basic, lightweight website following the trends of minimalism and web bloat reduction in the Linux community. Since then, we have grown into a platform focused on building applications that enrich people's lives through protecting their right to privacy while providing excellent user experiences.

To reflect this shift, we have rebranded and redesigned to provide a better experience for new users. This new design reflects our commitment to good user interface and experience design that is unmatched in our space. Easy to use, visually appealing applications are essential to the promotion of privacy.

Websites don't have to look basic to be lightweight. Using new technologies allows us to build new experiences faster and reduce the cost of maintainance. Thanks to these technologies, we now have instant page loads when clicking on links to other pages.

![](/img/posts/next-1.svg)

## No name

We decided to remove our previous name and switch to a simple logo. The previous name caused confusion for many people. Our domain name no longer stands for anything, we want to be associated with our products, not a name. Our new logo is a huge improvement over our previous images.

## Colors and gradients

We kept the same color palette while simplifying with less colors and adding a darker blue. Gradients give an interesting appearance to surfaces that would otherwise be flat and boring.

![](/img/posts/next-2.svg)