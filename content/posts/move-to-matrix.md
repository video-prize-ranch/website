+++
title = "Move to Matrix"
date = 2021-03-10

[extra]
desc = "A guide to switching to Matrix with frequently asked questions."
img = "/img/blog-default.svg"
+++

Matrix is a federated messaging service and a great alternative for communities. Discord communities can be bridged to Matrix to make migration easier. Because Matrix is federated, censoring users is almost impossible making it a great choice if you got banned from Discord. This guide will help you understand what Matrix is and how to move to it.

## Bringing your friends to Matrix

Getting people to switch will be hard, talking to people about privacy is like talking to a brick wall. Tell them about the benefits of Matrix and how easy it is to sign up and start talking without worrying about Discord's risks. Remember, getting people to switch is a process and will have a hard time. Here's some benefits to mention: (don't forget to share this article!)

- All the features of Discord, but without the data-mining or surveilance.
- Read receipts! See when your friend has read your messages (can be turned off)
- Hide message content from notifications and support for additional touch ID when you open the app, never worry about people snooping on your conversations when they have your phone.
- Free, open source software. Customize Element (or other client!) without getting banned for a ToS violation.
- Matrix has end-to-end encryption meaning the homeservers cannot see your messages
- If you get banned off a homeserver, just switch homeservers. (if they have been banned off Discord before)
- You'll never have to provide ID or any contact information (if they have been asked by Discord to provide ID or phone number)

Before asking a friend to switch, check if they are already on [Signal](https://signal.org/). While Signal is centralized, it doesn't leak as much metadata. Signal is not ideal for large groups or communities.

## Bringing over your community

You can use the [matrix-appservice-discord](https://matrix.org/docs/projects/bridge/matrix-appservice-discord) bridge to let people chat on both sides. This is a great way to transition people into Matrix. If you don't want to host the bridge yourself, t2bot.io has it hosted, just invite the Matrix bot to your room and follow the instructions [here](https://t2bot.io/discord/).

## VoIP

Matrix does not have voice channels, but group calls can be made using the Jitsi integration (Element only). All members of the room will be notified of the call. 1 to 1 calls are made using P2P WebRTC. Using [Jitsi Meet](https://jitsi.org/) by itself (can be used without video) or [Mumble](https://www.mumble.info/) may be better. Shift-click on the Video icon in Element for screen sharing.

## How do I sign up?

Using Element, go to [app.element.io](https://app.element.io/) or download the app from [element.io](https://element.io/). Click on "Create Account". Don't use matrix.org! It will be slow. Pick one from the list below and click on Edit to change it. Fill in the fields and click on Register. Instructions will be different on other clients.

### Public homeservers

- kde.org
- nitro.chat
- tchncs.de
- oper.io
- envs.net
- fairydust.space
- halogen.city
- ru-matrix.org
- chat.privacytools.io

### Clients

- [Element](https://element.io/) (Web, Windows, Linux, macOS, iOS, Android)
- [FluffyChat](https://fluffychat.im/) (Web, Windows, Linux, macOS, iOS, Android)
- [gomuks](https://github.com/tulir/gomuks) (Windows, Linux, macOS)
- [Syphon](https://www.syphon.org/) (iOS, Android)

## FAQ

### What is federation?

Federation is a way to decentralize. Think of it like email, you sign up with Gmail but you can email Outlook users.

### What is a room?

A room is like a Discord channel, you can send and receive messages in it. Something like Discord servers is ~~[coming soon](https://github.com/matrix-org/matrix-doc/pull/1772)~~ here, Spaces has launched in beta for the Element client.

### What is a homeserver?

A homeserver is where your data is stored and where your events (like messages) are sent, events are then sent to all the other homeservers in the room.

### How do I invite someone to a room?

If the room is public, copy the room ID (In Element, click the room name at the top and it will be under Room Addresses) and to go [matrix.to](https://matrix.to/) to create an invite link. Element has this built-in, just click the (i) symbol in the top right corner and click "Invite room" to invite a user or "Share room" to get a link, for private rooms it is the same. The buttons will be in different places with different clients.

### What is an MXID?

MXIDs are used to find rooms and users. For a channel, they start with # and for users they start with @. They will look something like this: #room:example.com

### What is the difference between Matrix and Element?

Element is a client for Matrix. There are also other clients like Fractal, Nheko, Syphon, and Neochat. Element also provides hosting services for you to have your own homeserver through Element Matrix Services.

### Why is it slow?

This is due to your homeserver, matrix.org is known to be slow because of the amount of users. You should a different homeserver like kde.org, chat.privacytools.io, halogen.city, tchncs.de, nitro.chat, oper.io, envs.net, or fairydust.space. Or [host your own](https://github.com/matrix-org/synapse/blob/develop/INSTALL.md) or use [Element Matrix Services](https://ems.element.io/).