+++
title = "Archiving Librarian"
date = 2023-05-07

[extra]
desc = "Librarian will no longer be maintained."
img = "/img/posts/archiving-librarian.avif"
+++

> Please note that any hateful comments will not be tolerated in our communities and will result in a **permanent ban**.

At bcow.xyz, we value privacy and open source, and part of this mission is inclusivity. We want to make sure that we protect those
who are marginalized in society, and promote real solutions to large tech monopolies. Privacy and open source cannot be separated 
from politics, and we need a fundamental restructuring of our economic system to achieve our goals. We can no longer tolerate the 
explosion of hate speech and extremism on the Odysee platform. Librarian will be archived and no longer maintained. Instance operators
should shut down their Librarian instances.

I'm not going to go into the details of this, as I have already written other blog posts that explain Odysee's issues in more
detail. I suggest you read [this one](/posts/i-regret-building-librarian/).

This comes decision is a result of The Linux Experiment's decision to leave Odysee. I have always felt uncomfortable about these
issues but it's important to stand up and take action when you feel something is wrong. You can read The Linux Experiment's blog
post [here](https://thelinuxexp.com/Im-leaving-odysee/). If you are a creator on Odysee, I encourage you to move away from it, and
delete your account by sending an email to [hello@odysee.com](mailto:hello@odysee.com). For alternatives to Odysee, there's PeerTube,
specifically the instance [TILvids](https://tilvids.com/), where The Linux Experiment also uploads now. PeerTube is built on ActivityPub,
the same technology behind Mastodon, and you can even follow and comment on PeerTube videos from Mastodon.

Staying on Odysee reflects badly on you. Your videos are shown beside Nazi conspiracy theories and misinformation while they receive 
extremely offensive comments. It's time to leave.

