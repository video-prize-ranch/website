+++
title = "User safety with Librarian"
date = 2023-03-19

[extra]
desc = "Odysee's environment has continued to worsen. Here are our recommendations."
img = "/img/blog-default.svg"
+++

> Librarian is no longer maintained due to these concerns. [Learn more >](/posts/archiving-librarian/)

Librarian provides an alternative way to access content on LBRY/Odysee. It has come to our attention that some of the content available is dangerous, hateful, or inappropriate. Librarian is not able to control or moderate content because it is only an alternative frontend. To mitigate these issues, we have placed warnings and notices to inform users, such as before being able to view comments.

## Our recommendations

To protect yourself from dangerous, hateful, or inappropriate content, we encourage you to reduce your usage of LBRY/Odysee, including through Librarian and use some of our tips below:

### 1. Avoid viewing comments

Comments are where most of this harmful content is. Avoid viewing comments and ignore comments about making money, these are scams.

### 2. Avoid using content discovery features

Related videos are disabled by default on most instances, these often uncover spam videos and are not useful. Odysee's search system is not very good, remember to check follower counts to make sure you are not viewing an imposter's account, and avoid using search to find specific videos, you are unlikely to get good results. While Odysee's featured videos are only shown from a list of channels that are curated by Odysee, "Rabbit Hole" is not, this may result in inappropriate or spam videos.

### 3. Find your favorite creators on alternative platforms

We understand that most of our users do not like YouTube, but your creators may be available on other platforms such as [PeerTube](https://joinpeertube.org/). We are creating a list that will let you see which creators are on PeerTube. Check video descriptions for other places to find their content.

### 4. Use Odysee?

Because Librarian is anonymous, we do not have a way to report content to Odysee. It might not be well enforced but Odysee has [community guidelines](https://help.odysee.tv/communityguidelines/). Make your voice heard and report content. However, only videos can be reported and not comments.

## What we're doing

I am going to focus more on other projects and contributing to open source. Because of these issues, Odysee will become less and less relevant over time as people do not want to use a platform that is not well moderated. The Librarian codebase would be a good starting point for a new YouTube frontend and this may happen.