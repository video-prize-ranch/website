+++
title = "Shutting down Tor hidden services"
date = 2022-06-10

[extra]
desc = "To ensure service quality, we are taking down our hidden services."
img = "/img/blog-default.svg"
+++

Our Tor hidden services run on a seperate server with a bad internet connection. This causes slow load times and a poor experience for those using the hidden service. We have tried to move the services in the past but failed, the new server would not be picked up by the Tor network and say "Onionsite not found". The poor internet connection would also result in many hours of downtime that made the service unreliable and we are unable to monitor hidden services using our status page. To ensure we provide a quality service for all our users, we will be turning off our Tor hidden services on June 18, 2022 and start redirecting to clearnet instances on June 12, 2022 (excluding Bibliogram and Nitter, which will redirect to this notice).

If you still want to keep using hidden service instances, you can find others on instance lists.

- [Librarian](https://codeberg.org/librarian/librarian#tor)
- [rimgo](https://codeberg.org/video-prize-ranch/rimgo#tor)
- [Bibliogram](https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md)
- [Nitter](https://github.com/zedeus/nitter/wiki/Instances#tor)