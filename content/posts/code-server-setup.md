+++
title = "Secure development environments with code-server"
date = 2023-08-18

[extra]
desc = "Keep your development tools separate for simplicity and security."
img = '/img/posts/code-server-setup.avif'
+++

With multiple incidents of malicious packages on npm, I decided to do all development inside a virtual machine. This protects my main system from malicious code and keeps development tools and packages isolated. Using [OpenGL acceleration](/posts/opengl-vm-acceleration.md), I was able to get a mostly smooth desktop experience. But this setup was unreliable, OpenGL acceleration is not a reliable solution and has broken multiple times with QEMU updates and different distributions.

## My new setup

I'm still using a virtual machine, but now it's a headless Arch Linux VM that runs [code-server](https://github.com/coder/code-server) and [Tailscale](https://tailscale.com/). This keeps my development tools isolated from my host system (which runs [Fedora Silverblue](https://silverblue.fedoraproject.org/), an immutable distro). I went with a virtual machine as it allows me to use docker and podman containers. code-server is a version of VS Code that runs inside a browser with everything running and stored on the server. Using Tailscale allows me to access code-server from anywhere securely, without dealing with complicated virtual machine networking. [Arch Linux](https://archlinux.org/) is the perfect Linux distro for development. It provides you with the latest versions of all the tools you need. And if what you need isn't in the official repos, there's a good chance it's in the [AUR](https://aur.archlinux.org/). You can follow the steps below to create your own!

## Setup guide

1. Install Virtual Machine Manager (virt-manager) or [GNOME Boxes](https://flathub.org/apps/org.gnome.Boxes) (available as a Flatpak for immutable distros)
2. Download the latest [Arch Linux ISO](https://archlinux.org/download/) (or your preferred distro, but these instructions are for Arch)
3. Create a new virtual machine using the ISO and change your CPU, memory and storage to your preferences.
4. Run `archinstall` to begin the installation process and follow the steps. I used the server profile and btrfs for file system.
5. Reboot the VM once the installation is complete.
6. Install an AUR helper such as paru:
  - `git clone https://aur.archlinux.org/paru-bin.git`
  - `cd paru-bin`
  - `makepkg -si`
7. Install Tailscale on your host and VM: `sudo pacman -S tailscale`
8. Setup Tailscale: `sudo systemctl enable --now tailscaled` and `sudo tailscale up`
9. Install and start code-server: `paru -S code-server` and `sudo systemctl enable --now code-server@$USER`
10. Open `~/.config/code-server/config.yaml` and change `bind-addr: 127.0.0.1:3000` to `bind-addr: 0.0.0.0:3000` and take note of the password
11. Restart code-server: `sudo systemctl restart code-server@$USER`
12. Open a browser and enter: `http://[tailscale device name]:3000`
13. 🎉 You are now ready to start coding!