+++
title = "The fall of alternative frontends"
date = 2023-07-03

[extra]
desc = "Alternative frontends are facing resistance, but we must continue."
img = "/img/the-fall-of-alternative-frontends.avif"
+++

It seems that most of the major alternative frontend projects are facing resistance from their respective upstream services. Invidious and Piped are being rate limited by YouTube (and receiving angry emails), Libreddit can no longer use the Reddit API, and Nitter has been affected by the new account requirement to access Twitter. This highlights that alternative frontends are only a temporary solution while we transition to open source, non-profit alternatives like the Fediverse.

These services are cracking down on lost revenue due to the inability to show ads and track users. We believe that privacy is a fundamental right and that the advertising model of the Internet must be dismantled. We cannot continue to fight for privacy while continuing to use for-profit services. The capitalist profit motive is fundamentally incompatible with privacy, so we must move to community-controlled, non-profit alternatives. The desire for ever-increasing user monetization is at odds with a good, private user experience.

It is clear that YouTube is trying to recoup lost ad revenue by shutting down alternative frontends and preventing the use of ad blockers. YouTube's legal team has contacted Invidious for "terms of service violations" and has rate-limited instances of the software, including another similar service called Piped. This rate limiting causes excessive buffering and errors, making Piped and Invidious unusable most of the time. Their actions not only target these projects, but also ad blockers. YouTube will soon block video playback for those using ad blockers. All of this is happening as YouTube increases the number of ads and makes it impossible for users to skip ads.

Another frontend for a Google service has met resistance: the Aurora Store allows you to download apps from the Google Play Store without a Google account, a crucial service for those using de-Googled Android systems. Google has been rate limiting the accounts used for the Aurora Store, causing issues with the app, and forcing users to use the official Google Play Store with a Google account. This allows Google to continue tracking app downloads and serve ads in the Play Store.

On Reddit, users have been protesting the new API pricing, which has forced third-party apps like Apollo to shut down. However, their efforts have been unsuccessful in stopping Reddit from implementing the new pricing, which has been criticized for being extremely overpriced. This forces users to use the notoriously problematic official Reddit app, which allows Reddit to display ads. While alternative frontends such as Libreddit continue to function, it may soon cease to function without the move to Reddit's private APIs. This has led to a large migration to Fediverse platforms like Lemmy and kbin.

There is hope for a better internet. The Fediverse is a shining example of how open source, community-driven, non-profit technology can successfully create an alternative to Big Tech platforms. Mastodon, an alternative to Twitter, grew by hundreds of thousands of users as a result of Elon Musk's changes to Twitter. Lemmy, an alternative to Reddit, is growing rapidly due to Reddit's API changes. However, PeerTube has so far been unsuccessful in attracting quality content creators, likely due to its non-profit nature. If we want PeerTube to succeed, we need to eliminate the need for content creators to make a living from their content.

We have a lot of work to do. We have a lot of work to do, but we must be reminded that our actions are contributing to the creation of a better Internet, where users are not tracked and sold for the enrichment of large corporations. We must continue to make our vision of the Internet a reality by improving our services and frontends.

Further reading: [Pluralistic: Tiktok's enshittification](https://pluralistic.net/2023/01/21/potemkin-ai/)