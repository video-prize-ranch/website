+++
title = "How to sign in to Tailscale with Codeberg"
date = 2025-03-07

[extra]
desc = "Sign into Tailscale without Big Tech."
img = "/img/posts/tailscale-codeberg.svg"
+++

![](/img/posts/tailscale-codeberg.svg)

The [official Tailscale documentation](https://tailscale.com/kb/1240/sso-custom-oidc) for setting up a Custom OIDC provider is very unclear and hard to follow, so I thought I would make this step by step guide on how to do it with Codeberg. With this, you can use Tailscale without any Big Tech accounts.

**Requirements**

- Custom domain with email
- Website on the same domain to add the WebFinger file
- Codeberg account

## Setting up your domain

You will need to be able to receive emails at your domain to verify it on your Codeberg account. This setup requires a custom domain as you will need to create a WebFinger file to tell Tailscale what identity provider to use.

### Adding the WebFinger file

You will need to create the following file at `https://your-domain.com/.well-known/webfinger`. If you don't already have a website on your domain, you can easily upload the file to a repo on Codeberg and setup [Codeberg Pages](https://codeberg.page/).

```json
{
  "subject": "acct:email@your-domain.com",
  "links": [
    {
      "rel": "http://openid.net/specs/connect/1.0/issuer",
      "href": "https://codeberg.org/"
    }
  ]
}
```

## Setting up your Codeberg account

### Create an application on Codeberg

You will need to create an OAuth2 application from the Codeberg settings page.

1. Sign into [Codeberg](https://codeberg.org).
2. Click on your profile picture and then Settings.
3. In the left sidebar, click Applications and scroll to the bottom.
4. Enter an application name (such as "tailscale"), and for redirect URIs, use `https://login.tailscale.com/a/oauth_response`. Keep "Confidential client" checked.
5. Take note of the client ID and secret, you will need this for Tailscale.

### Make your custom domain email your primary email

Your custom domain email needs to be added to your Codeberg account as your primary email.

1. In settings, click on Account in the left sidebar.
2. Enter your email and click "Add email address".
3. Verify your email.
4. Click "Make primary".

> ⚠️ Do not remove your previous email. This will cause your git commits to be unlinked to your account.

## Sign up at Tailscale

1. Go to login. and click "Sign up with OIDC"
2. Enter your custom domain email and choose Codeberg as your identity provider.
3. Enter your client ID and secret, leave everything else as the default and continue.
4. You should now be able to sign in with Codeberg and you will be redirected to the Tailscale admin console.
5. 🎉 That's it!

## Notes

- If you have an existing Tailscale account on a custom domain, you can request for the identity provider to be migrated. However, accounts on shared domains such as @gmail.com or @outlook.com cannot be migrated.
- You can do this on the free Personal plan. After sign up, you can change your plan in the Tailscale settings to end your trial immediately.
- If you followed along with this guide, you can probably handle self-hosting your own Tailscale with Headscale.
