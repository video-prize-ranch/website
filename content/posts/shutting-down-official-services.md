+++
title = "Shutting down official services"
date = 2023-01-15

[extra]
desc = "I got banned."
img = "/img/blog-default.svg"
+++

I was banned from Fly.io, I don't know why but i'm guessing that privacy is suspicious. The official instances for rimgo and Librarian will not be restored. There are quality instances hosted by the community and listed in the READMEs) and keeping the instances down will allow me to focus on development.

You can find community hosted instances on Codeberg and on the instance selection pages below:
- [Librarian](https://librarian.codeberg.page/)
- [rimgo](https://rimgo.codeberg.page/)

This all started because I decided to reinvent instance selection pages. Instance selection pages usually provide too much information and don't look good. An important metric for selecting an instance is the uptime. I initially checked uptime using Codeberg's CI system, but this frequently failed and occupied a limited amount of build slots. 

That got me banned from Codeberg CI, so I self-hosted [Woodpecker](https://woodpecker-ci.org/), the open source project that powers Codeberg CI. I hosted the agents using Fly.io which was difficult and forced me to keep deploying new versions to try to get it working, it took multiple days and I never got it to work fully. This is likely what resulted in the ban. I just decided to give up on uptime checking.

Hosting is easily the most stressful part of operating these privacy frontends. Because many providers get abused, they develop fraud prevention systems that also catch those who value their privacy. While i'm not sure this is the reason, it likely is. I have received a full refund from Fly.io and I will try to donate this to the people who host my frontends.