+++
title = "Privacy Policy Updates"
date = 2022-08-05

[extra]
desc = "We have updated our privacy policy to allow temporary request logging for diagnostics."
img = "/img/blog-default.svg"
+++

A bot has been requesting hundreds of thousands of images from Imgur using rimgo, this created over 300 GB of data transfer in just 2 days. Thankfully, Fly.io's bandwidth pricing is much cheaper than AWS, where this would have costed $27. To investigate, we enabled Amazon CloudFront. CloudFront also provides us with anonymous usage statistics. Unfortunately, anonymous statistics weren't enough and we had to temporarily enable request logging, a violation of our privacy policy. Logging was only enabled temporarily for 2 periods of around 5 minutes at 10 AM ET. These logs have been deleted. I have also reviewed a live output of logs for the past two hours to ensure that the bot was effectively blocked, these were never written to disk and I was only able to see the most recent 3 requests. All logging has been disabled and I apologize for this breach of trust that you have placed in us.

We have implemented filters to block non-browser clients and will be implementing further restrictions such as rate-limiting. To continue monitoring the situation, CloudFront will remain in place until we can confirm that this has stopped. We recommend using other instances to distribute the load and find others who you trust. Librarian and rimgo both have an instance privacy feature to see what data an instance collects, but from this incident, you will see that this can be false or be ignored.

Our privacy policy has been updated to show that we will log for diagnostics but that these logs will be destroyed and only kept as long as necessary to fix an issue. I recommend you to look at the instance lists for [Librarian](https://codeberg.org/librarian/librarian#instances) and [rimgo](https://codeberg.org/video-prize-ranch/rimgo#instances) to find alternative instances if you no longer trust us and to help reduce load on our instance.