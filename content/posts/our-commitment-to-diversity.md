+++
title = "Our commitment to diversity"
date = 2024-06-09

[extra]
desc = "bcow.xyz is committed to creating a better Internet for everyone."
img = "/img/posts/our-commitment-to-diversity.svg"
+++

![Our logo on a rainbow background](/img/posts/our-commitment-to-diversity.svg)

At bcow.xyz, we are committed to fostering a safe and welcoming environment for everyone, including those who identify as 2SLGBTQIA+. As we celebrate Pride Month, it is important to reflect on the values of diversity, inclusion and equality. This year, we are committed to continuing to support these values in our mission to promote safe and private access to information.

We acknowledge that some of our previous work has not benefited people of color, people who identify as 2SLGBTQIA+, and more. However, we are committed to overcoming this by continuing to enforce our Code of Conduct, calling out hateful content and the platforms that enable it, and donating to organizations that support equality.

We believe that everyone deserves the right to privacy, the right to express themselves, and the right to be free from harassment. Our current and future projects will work to advance these rights and create a better world for all. A world where everyone is free to be themselves.

🏳️‍🌈 🏳️‍⚧️