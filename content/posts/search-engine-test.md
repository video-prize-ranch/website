+++
title = "Search Engine Test"
date = 2021-11-15

[extra]
desc = "This is a review of all the big privacy respecting search engines."
img = "/img/blog-default.svg"
+++

This is a review of all the big privacy respecting search engines. Google is included to compare to. This review is based on what the search engines gave without going to external websites.

## Grading system
- `A` - Exceeds expectations, better than Google's results
- `B` - Meets expectations, displays the same as Google
- `C` - Does not meet expectations, displays limited information compared to Google
- `D` - Shows no information, forces user to go to external website

## Searx
For searx, the Docker image was used with no settings modified.

> Brave Search is currently in beta and things may change when it is fully released.

# Things

# `donald trump`
## Google
Displayed all relevant information such as a website, net worth, date of birth, height, party, spouse, children, and social media profiles.

## `B` DuckDuckGo
Displays all relevant information.

## `B` Brave Search
Displays all relevant information.

## `B` Whoogle
Displays all relevant information.

## `B` Startpage
Displays all relevant information.

## `B-` Searx
Displays some information such as date of birth, social media profiles, and height.

## `D` Qwant
Qwant failed to display any information.

# `yacy`
## Google
Displayed all relevant information such as a description (from Wikipedia), logo, author, and license.

## `A` Brave Search
Brave displayed the same information as Google along with social media profiles and Linux packages.

## `B+` DuckDuckGo
Displayed the same information as Google with links to their website and Twitter page.

## `B+` Searx
Displayed the same information as Google with links to their website and Twitter page.

## `B-` Whoogle
Whoogle displayed the same information as Google but forced the user to scroll down to see the infobox.

## `C` Qwant
Qwant failed to display extended information such as the license, author, and programming language. The full description is also not given and forces users to go to Wikipedia.

## `C` Startpage
YaCy displayed a description from Wikipedia but did not provide any additional information such as the author or license.

# Utilities
## `define search`
### Google
Google displayed the definition, a button to say the word, definitions by topic and verbs, nouns, and adjectives.

### `B-` DuckDuckGo
DuckDuckGo displayed the definitions as a verb and noun with no example sentences and synonyms but did have a button to say the word.

### `B-` Startpage
Whoogle displayed the definitions as a verb and noun with example sentences and synonyms but did not have a button to say the word or definitions by topic.

### `B-` Whoogle
Whoogle displayed the definitions as a verb and noun with example sentences and synonyms but did not have a button to say the word or definitions by topic.

### `C` Brave Search
Brave Search displayed definitions but without any additional information.

### `D` Qwant
Qwant failed to display any information.

### `D` Searx
Searx failed to display any information.


## `1 EUR to USD`
### Google
Google displayed the current exchange rate in the autocomplete and search results. Google also displayed a graph in search results.

### `B` Startpage
Startpage displayed the current exchange rate and a graph with options to change the date range.

### `B-` Brave Search
Brave Search displayed the current exchange rate and a graph with no options for the date range.

### `C` DuckDuckGo
DuckDuckGo displayed the current exchange rate but clicking on "View Chart" takes user to an external website.

### `C` Searx
Searx displayed the current exchange rate but did not display a graph.

### `C` Whoogle
Whoogle displayed the current exchange rate but did not display a graph.

### `D` Qwant
Qwant failed to display any information.

## `1 kg to lbs`
### Google
Google displayed the conversion.

### `B` Brave Search
Brave Search displayed the conversion.

### `B` DuckDuckGo
Brave Search displayed the conversion.

### `B` Whoogle
Whoogle displayed the conversion.

### `B-` Startpage
Startpage failed to display the conversion when `1 kg to lbs` was used but showed the conversion when `1 kg to lb` was used, the conversion was below a translation of `1 kg`.

### `D` Qwant
Qwant failed to display any information.

### `D` Searx
Searx failed to display any information.

## `1+1`
### Google
Google displayed the answer in a calculator.

### `B` Brave Search
Brave Search displayed the answer in a calculator.

### `B` DuckDuckGo
DuckDuckGo displayed the answer in a calculator.

### `B` Startpage
Startpage displayed the answer in a calculator.

### `B-` Qwant
Qwant displayed the answer but not a calculator.

### `D` Searx
Searx fails to display the answer and instead shows a Ukrainian TV channel.

## `hello in french`
### Google
Google displayed the translation and alternative words.

### `B` DuckDuckGo
DuckDuckGo displayed the translation.

### `B` Startpage
Startpage displayed the translation.

### `B` Whoogle
Whoogle displayed the translation.

### `D` Brave Search
Brave Search failed to display any information.

### `D` Searx
Searx failed to display any information.

### `D` Qwant
Qwant failed to display any information.

## `time in moscow`
### Google
Google displayed the current date and time in Moscow and the time zone.

### `B` Brave Search
Brave Search displayed the current date and time in Moscow and the time zone.

### `B` DuckDuckGo
DuckDuckGo displayed the current date and time in Moscow and the time zone.

### `B-` Whoogle
Whoogle displayed the current date and time in Moscow but not the time zone.

### `C` Startpage
Startpage displayed the current time in Moscow but not the date or time zone.

### `D` Qwant
Qwant failed to display any information.

### `D` Searx
Searx failed to display any information.


## `weather in montreal`
### Google
Google displayed the expected information such as the weather conditions (sunny, raining, etc.), temperature, 7-day forecast, chance of precipitation, humidity, and wind.

### `A` Startpage
Startpage displayed the same information as Google but included air quality.

### `B` Brave Search
Brave displayed the same information as Google.

### `B` Qwant
Qwant displayed the same information as Google.

### `B-` DuckDuckGo
DuckDuckGo displayed the same information as Google but requires the user to click on the "Weather" tab before showing the information.

### `C` Whoogle
Whoogle displayed basic weather information such as the temperature and weather conditions but failed to give a 7-day forecast or wind and humidity.

### `D` Searx
Searx failed to display any weather information.


# Movies and TV

## `squid game`
### Google
Displayed all relevant information such as cast, a description, genres, episodes, ratings, and streaming service.

### `B-` Whoogle
Whoogle displayed all relevant information such as a list of episodes and the cast, but failed to display which streaming service.

### `C` Brave Search
Displayed information such as production and release information but failed to provide a list of episodes.

### `C` DuckDuckGo
DuckDuckGo failed to display important information such as the cast and list of episodes. Only a description, genre, and creator were shown.

### `C` Searx
Searx failed to display important information such as the cast and list of episodes. Only some information was shown such as a description, country of origin, genre, and director. 

### `C-` Startpage
Startpage failed to display important information such as the cast and list of episodes. Only a description was provided.

### `D` Qwant
Qwant failed to show any relevant information and instead provided Squid Game related videos.