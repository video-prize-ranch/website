build:
  tailwindcss -i ./static/tailwind.css -o ./static/app.css -m
  zola build

dev:
  tailwindcss -i ./static/tailwind.css -o ./static/app.css -m -w &
  zola serve --port 3000

push: build
  mv public /tmp/public-aoomqw
  git branch -D pages
  git switch --orphan pages
  rm -rf *
  mv /tmp/public-aoomqw/* .
  mv /tmp/public-aoomqw/.well-known .
  mv /tmp/public-aoomqw/.domains .
  rm -rf /tmp/public-aoomqw
  git add .
  git commit -m "Update site"
  git push --force --set-upstream origin pages
  git checkout main
