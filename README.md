# Website
The fourth version of website, built with Zola.

## Requirements
- [Zola](https://www.getzola.org/)
- [Tailwind CLI](https://tailwindcss.com/blog/standalone-cli)

## Building
```sh
tailwindcss -i ./static/tailwind.css -o ./static/app.css -m
zola build
```

To build and push to Codeberg Pages, use the `build.sh` script.

## Development
Run the following in separate terminals.
```sh
tailwindcss -i ./static/tailwind.css -o ./static/app.css --watch
zola serve
```